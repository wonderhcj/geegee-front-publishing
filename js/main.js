window.onload = function() {

	var elem = document.getElementById('GeeGee'),
    marker = true,
    oneStart = true,
    downcheck = false,
    delta,
    direction,
    interval = 100,
    counter1 = 0,
    counter2 = 0;
    count = 0;
    textMotion = false;


	var lineBox = document.getElementById("lineBox");
  	var firstList = document.getElementById("firstList");
  	var middleList = document.getElementById("middleList");
  	var lastList = document.getElementById("lastList");
  	var motionInfo = document.getElementById("motionInfo");
  	var textH = document.getElementById("textH");
  	var textM = document.getElementById("textM");
  	var textB = document.getElementById("textB");
  	var textButton = document.getElementById("textButton");
  	var step01= document.getElementById("step01");
  	var step02= document.getElementById("step02");
  	var step03= document.getElementById("step03");



	if (elem.addEventListener) {
		elem.addEventListener("mousewheel", wheel, false);
		elem.addEventListener("DOMMouseScroll", wheel, false);
	}
	else elem.attachEvent("onmousewheel", wheel);
	function wheel(e){
	  counter1 += 1;
	  e = window.event || e;
	  delta=e.detail? e.detail*(-120) : e.wheelDelta
	  if (delta>0) {direction = 'up';} else {direction = 'down';}
	  if (marker) wheelStart();
	  console.log(direction)
	  return false;
	}
	function wheelStart(){
	  marker = false;
	  wheelAct();
	}
	function wheelAct(){
	  counter2 = counter1;
	  setTimeout(function(){
	    if (counter2 == counter1) {
	    	if (direction == 'down'){
	    		wheelEndDown();
	    	} else {
	    		wheelEndUp();
	    	}
	    } else {
	      wheelAct();
	    }
	  },interval);
	}


	function wheelEndUp(){
	  marker = true,
	  counter1 = 0,
	  counter2 = 0;
	  -- count;
	  oneStart = false;

		if (count === 0 || count === -1){
			count = 0
		}

		console.log(count)

		if (count === 1 && direction == 'up') {
			
			var infoWrap = document.getElementsByClassName("infoWrap");
  			for( var i = 0; i < infoWrap.length; i++ ){
			    var infoTopMotion = infoWrap.item(i);
			    infoTopMotion.classList.remove('infoFotMotion');
			    infoTopMotion.classList.add('infoFotMotionUp');
			}
			var main = document.getElementsByClassName("main");
  			for( var i = 0; i < main.length; i++ ){
			    var main = main.item(i);
			    main.classList.remove('mainLastFot');
			    main.classList.add('mainFotUp');
			}
			var motionConts = document.getElementsByClassName("motionConts");
  			for( var i = 0; i < motionConts.length; i++ ){
			    var motionConts = motionConts.item(i);
			    motionConts.classList.remove('motionContsLastFot');
			    motionConts.classList.add('motionContsFotUp');
			}
			var footer = document.getElementsByClassName("footer");
  			for( var i = 0; i < footer.length; i++ ){
			    var footer = footer.item(i);
			    footer.classList.remove('footerLastFot');
			    footer.classList.add('footerFotUp');
			}
		}

		if (count === 0 && direction == 'up') {

			textH.classList.remove('textH');
			textM.classList.remove('textM');
			textB.classList.remove('textB');

			step01.classList.remove('step01');
			step02.classList.remove('step02');
			step03.classList.remove('step03');

			lineBox.classList.remove('lineBox');
			firstList.classList.remove('firstList');
			middleList.classList.remove('middleList');
			lastList.classList.remove('lastList');

			if (textMotion){
				var infoWrap = document.getElementsByClassName("infoWrap");
				for( var i = 0; i < infoWrap.length; i++ ){
				    var infoBottomMotion = infoWrap.item(i);
				    infoBottomMotion.classList.remove('infoTopMotion');
				    infoBottomMotion.classList.remove('infoFotMotionUp');
				    infoBottomMotion.classList.remove('infoBottomMotion');
				}
			}

			var main = document.getElementsByClassName("main");
  			for( var i = 0; i < main.length; i++ ){
			    var main = main.item(i);
			    main.classList.remove('mainLastFot');
			    main.classList.remove('mainFotUp');
			}
			var motionConts = document.getElementsByClassName("motionConts");
  			for( var i = 0; i < motionConts.length; i++ ){
			    var motionConts = motionConts.item(i);
			    motionConts.classList.remove('motionContsLastFot');
			    motionConts.classList.remove('motionContsFotUp');
			}
			var footer = document.getElementsByClassName("footer");
  			for( var i = 0; i < footer.length; i++ ){
			    var footer = footer.item(i);
			    footer.classList.remove('footerLastFot');
			    footer.classList.remove('footerFotUp');
			}



		}
	}

	function wheelEndDown(){
		marker = true,
		counter1 = 0,
		counter2 = 0;
		++ count;
		oneStart = false;
		if (count === 3){
			count = 2
		}
  		console.log(count)

  		if (count === 1 && direction == 'down') {
  			textMotion = true;
  			firstList.classList.add('firstList');
  			middleList.classList.add('middleList');
  			lastList.classList.add('lastList');
  			var infoWrap = document.getElementsByClassName("infoWrap");
  			for( var i = 0; i < infoWrap.length; i++ ){
			    var infoTopMotion = infoWrap.item(i);
			    infoTopMotion.classList.remove('infoBottomMotion');
			    infoTopMotion.classList.add('infoTopMotion');
			}
			textH.classList.add('textH');
			textM.classList.add('textM');
			textB.classList.add('textB');
			lineBox.classList.add('lineBox');
			step01.classList.add('step01');
			step02.classList.add('step02');
			step03.classList.add('step03');

		}

		if (count === 2 && direction == 'down') {
			
			var infoWrap = document.getElementsByClassName("infoWrap");
  			for( var i = 0; i < infoWrap.length; i++ ){
			    var infoTopMotion = infoWrap.item(i);
			    infoTopMotion.classList.remove('infoBottomMotion');
			    infoTopMotion.classList.remove('infoTopMotion');
			    infoTopMotion.classList.remove('infoFotMotionUp');
			    infoTopMotion.classList.add('infoFotMotion');
			}
			var main = document.getElementsByClassName("main");
  			for( var i = 0; i < main.length; i++ ){
			    var main = main.item(i);
			    main.classList.remove('mainFotUp');
			    main.classList.add('mainLastFot');
			    
			}
			var motionConts = document.getElementsByClassName("motionConts");
  			for( var i = 0; i < motionConts.length; i++ ){
			    var motionConts = motionConts.item(i);
			    motionConts.classList.remove('motionContsFotUp');
			    motionConts.classList.add('motionContsLastFot');
			}
			var footer = document.getElementsByClassName("footer");
  			for( var i = 0; i < footer.length; i++ ){
			    var footer = footer.item(i);
			    footer.classList.remove('footerFotUp');
			    footer.classList.add('footerLastFot');
			}
	
		}
	}


	// Start the animation with JavaScript
	function MyLineBoxDown() {
	  lineBox.style.WebkitAnimation = " lineBoxDown 0.6s cubic-bezier(0.250, 0.460, 0.450, 0.940) both "; 
	  lineBox.style.animation = " lineBoxDown 0.6s cubic-bezier(0.250, 0.460, 0.450, 0.940) both "; 
	}MyLineBoxDown();
	// Standard syntalineBox
	lineBox.addEventListener("animationend", lineBoxMotion);
	// Code for Chrome, Safari and Opera
	lineBox.addEventListener("webkitAnimationEnd", lineBoxMotion);

	function lineBoxMotion(){
		setTimeout(function() {
			function MyLineBoxUp() {
  				lineBox.style.WebkitAnimation = " lineBoxUp 0.6s cubic-bezier(0.250, 0.460, 0.450, 0.940) both "; 
		  		lineBox.style.animation = " lineBoxUp 0.6s cubic-bezier(0.250, 0.460, 0.450, 0.940) both ";
			}MyLineBoxUp();
		}, 1000);
		function motionfirstList() {
			firstList.style.WebkitAnimation = " motionfirstList 1.2s cubic-bezier(0.250, 0.460, 0.450, 0.940) both";
			firstList.style.animation = " motionfirstList 1.2s cubic-bezier(0.250, 0.460, 0.450, 0.940) both";
		}motionfirstList();
		function motionMiddleList() {
			middleList.style.WebkitAnimation = " motionMiddleList 1.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) both";
			middleList.style.animation = " motionMiddleList 1.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) both";
		}motionMiddleList();
		function motionLastList() {
			lastList.style.WebkitAnimation = " motionLastList 1.8s cubic-bezier(0.250, 0.460, 0.450, 0.940) both";
			lastList.style.animation = " motionLastList 1.8s cubic-bezier(0.250, 0.460, 0.450, 0.940) both";
		}motionLastList();
		function motionTextH() {
			textH.style.WebkitAnimation = " textH 1.5s cubic-bezier(.23,1,.32,1) both";
			textH.style.animation = " textH 1.5s cubic-bezier(.23,1,.32,1) both";
		}motionTextH();
		function motionTextM() {
			textM.style.WebkitAnimation = " textM 2s cubic-bezier(.23,1,.32,1) both";
			textM.style.animation = " textM 2s cubic-bezier(.23,1,.32,1) both";
		}motionTextM();
		function motionTextB() {
			textB.style.WebkitAnimation = " textB 2.5s cubic-bezier(.23,1,.32,1) both";
			textB.style.animation = " textB 2.5s cubic-bezier(.23,1,.32,1) both";
		}motionTextB();
		function motionTextButton() {
			textButton.style.WebkitAnimation = " textButton 2.5s cubic-bezier(.23,1,.32,1) both";
			textButton.style.animation = " textButton 2.5s cubic-bezier(.23,1,.32,1) both";
		}motionTextButton();
	}


	


}